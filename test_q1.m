function s1 = test_q1()
A = input('input your function please');
if A == rref(A)
    disp('This is already a reduce echelon form')
else
    x = tril(A,-1);% get the lower triangular matrix of A
    if all(x(:) == 0) == 1 %if all elements in lower triangular matrix is not 0, it should not be a echelon form
        disp('it is a echelon form')
    else
        disp('it is not a echelon form')
    end
end
end