function w2 = inverse_matrix()
choice = input('Type "1" to read file and Type "2" to input matrix here!');

if choice == 1
    A = load('project2_q2.txt'); % read the matrix from file
else
    A = input('input your function please  '); %type your own matrix here
end
[m,n] = size(A);
if m~=n
    disp('Should be Square Matrix')
elseif det(A) == 0
    disp('This matrix is Singular and has no inverse matrix')
else
    choice2 = input('Type "1" to show inverse and Type "2" to write to txt file')
    if choice2 == 1
        inv(A) %caculate the inverse of A
    else
        dlmwrite('blank file.txt',inv(A)) %write your outcome of inverse to file and save.
        disp('Successful to write your inverse matrix in file!')
    end
end
end