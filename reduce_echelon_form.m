function s3 = reduce_echelon_form_q3()
A = input('input your function please');
[m,n] = size(A); % m is # of rows, and n is # of columns
x = tril(A,-1);% get the lower triangular matrix of A
if all(x(:) == 0) == 0 %if all elements in lower triangular matrix is not 0, it should not be a echelon form
    disp('it is not a echelon form')
    
else
    for i = 1:m
        A(i,:) = A(i,:)/A(i,i);%make each pivot to 1
        for j = 1:n
            if i ~= j %make every element in pivot column to 0
                A(j,:) = A(j,:) - A(i,:)*A(j,i);
            end
        end
        A;
    end
end
end

