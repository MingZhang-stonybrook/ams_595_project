
function findpi = find_pi(n)
ax1 = subplot(3,1,1); %make 3 figures in one page
n = input('Please input the trials you want to make'); %to make numbers of trials
pos = [0 0 1 1];
angle = linspace(0,pi/2,90);
x = cos(angle);
y = sin(angle);
plot(ax1,x,y,'b');% draw 1/4 blue circle 
axis('equal');
hold on 
rectangle('Position',pos,'EdgeColor','r'); %draw a square
x1 = rand(1,n);
y1 = rand(1,n);
plot(x1,y1,'gp')% draw green points in the graph
count1 = 0;
for i = 1:n
tic;
    if ((x1(1:end,i))^2+(y1(1:end,i))^2 < 1) %if the point inside the circle its distance should be less than or equal to 1.
        count1 = count1 + 1;
    end
    time = toc; % calculate the time we should take
end
findpi = 4*count1/n; %estimate pi.
difference = abs(findpi-pi); %check the precision
disp(['estimated pi is ',num2str(findpi),' and time is ',num2str(time),' the difference between the estimated pi and true pi is ',num2str(difference)])
title((['estimated pi is ',num2str(findpi),' and time is ',num2str(time),' the difference between the estimated pi and true pi is ',num2str(difference)]));
hold off
ax2 = subplot(3,1,2);
ax3 = subplot(3,1,3);
i1 = 0;
i2 = 0;
while i1 <= n
    count2 = 0;
    tic;
    for i = 1:n
        if ((x1(1:end,i))^2+(y1(1:end,i))^2 < 1)
            count2 = count2 + 1;
        end
    end
    
    i1 = i1 + 10;%set interval =10, and trace the change every 10 trials
    i2 = i2 + 1;
    preci(i2) = (4*count2/i1)-pi;% precision
    timecum(i2) = toc; %time consuming
plot(ax2,1:i2,timecum)
ylabel(ax2,'Computational Time')
xlabel(ax2,'number of trials(should times 10 here)')
plot(ax3,1:i2,preci)
ylabel(ax3,'Precision')
xlabel(ax3,'number of trials(should times 10 here)')
ylim([0 1])
end
end


function usewhilepi = find_pi_2(precision)
precision = input('what is the precision you choose, for example, 0.01,0.001 .ect?');
pos = [0 0 1 1];
angle = linspace(0,pi/2,90);
x = cos(angle);
y = sin(angle);
plot(x,y);
axis('equal');
hold on 
rectangle('Position',pos,'EdgeColor','r');
m = 100000;
matrix = rand(m,2);
i = 0;
j = 1;
tic;
while j <= m
    
    if (matrix(j,1))^2+(matrix(j,2))^2 <= 1
        i = i + 1;
        plot(matrix(j,1),matrix(j,2),'rp');% inside the circle red points.
    else 
        plot(matrix(j,1),matrix(j,2),'bp');% outside the circle blue points.
        
    end
    j = j + 1;

    if i/j - (i-1)/(j-1) <= precision
        break    % if the difference less than or equal to our precision, then skip the loop and get the result.       
    end
    

end
time = toc;
usewhilepi = 4*i/j;
i
title((['estimated pi is ',num2str(usewhilepi),' , and time is ',num2str(time),'s, and the precision is ',num2str(precision),'. Also, we need ',num2str(i),' steps to complete!']));
hold off
end
  
    
        
    

