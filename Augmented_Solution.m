function s4 = Augmented_Solution()
A = input('input your function please');
b = input('input your augment b please');
Augmented = [A b]; %To make the matrix become the Augmented matrix so that is can directly get the Answer.
[m,n] = size(A); % m is # of rows, and n is # of columns
tic
if m ~= n  || det(A) == 0
    disp('The solution is not possible')
else
    for i = 1:m
        Augmented(i,:) = Augmented(i,:)/Augmented(i,i);
        for j = 1:n
            if i ~= j
                Augmented(j,:) = Augmented(j,:) - Augmented(i,:)*Augmented(j,i);
            end
        end
    end
    s4 = Augmented(:,n+1);
end
time = toc; %calculate the time consumed
disp(['the number of rows is ',num2str(m),' and the number of column is ',num2str(n),'. Operating time is ', num2str(time),' s'])
end
