function s2 = echelon_form_q2()
A = input('input your function please  ');
[m,n] = size(A); % m is # of rows, and n is # of columns
for i = 1:m
    A(i,:) = A(i,:)/A(i,i);%make each pivot to 1
    for j = 1:n
        if i ~= j && i <j %make every element below the pivots become 0
            A(j,:) = A(j,:) - A(i,:)*A(j,i);
        end
    end
end
A
end